#define _BSD_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <time.h>
#include <sys/times.h>
#include <sys/vtimes.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <X11/Xlib.h>

#include <mpd/client.h>
#include <mpd/status.h>
#include <mpd/entity.h>
#include <mpd/search.h>
#include <mpd/tag.h>

char *tzpst = "Europe/Lisbon";

static Display *dpy;

#define MEMFILE "/proc/meminfo"
#define MPDHOST "localhost"
#define MPDPORT 6600

char *
smprintf(char *fmt, ...)
{
    va_list fmtargs;
    char *ret;
    int len;

    va_start(fmtargs, fmt);
    len = vsnprintf(NULL, 0, fmt, fmtargs);
    va_end(fmtargs);

    ret = malloc(++len);
    if (ret == NULL) {
        perror("malloc");
        exit(1);
    }

    va_start(fmtargs, fmt);
    vsnprintf(ret, len, fmt, fmtargs);
    va_end(fmtargs);

    return ret;
}

void
settz(char *tzname)
{
    setenv("TZ", tzname, 1);
}

char *
get_mpd_status()
{
    unsigned int i = 0;
    const struct mpd_song *song;
    const char *artist; 
    const char *title;
    int have_song = 0;
    struct mpd_connection *conn;
    conn = mpd_connection_new(MPDHOST,MPDPORT, 30000);
    if (mpd_connection_get_error(conn) != MPD_ERROR_SUCCESS) {
        return smprintf("%s", "Not connected");
        mpd_connection_free(conn);
    }
    mpd_command_list_begin(conn, true);
    mpd_send_current_song(conn);
    mpd_command_list_end(conn);

    while ((song = mpd_recv_song(conn)) != NULL ) {
        have_song = 1;
        artist = mpd_song_get_tag(song, MPD_TAG_ARTIST,i++);
        artist = (artist != NULL)?artist:"Unknown";
        i = 0;
        title = mpd_song_get_tag(song, MPD_TAG_TITLE, i++);
        title = (title != NULL)?title:"Unknown";
    };
    mpd_connection_free(conn);
    if (have_song == 1) {
        return smprintf("%s - %s |", artist, title);
    } else {
        return smprintf("");
    };

}

char *
mktimes(char *fmt, char *tzname)
{
    char buf[129];
    time_t tim;
    struct tm *timtm;

    bzero(buf, sizeof(buf));
    settz(tzname);
    tim = time(NULL);
    timtm = localtime(&tim);
    if (timtm == NULL) {
        perror("localtime");
        exit(1);
    }

    if (!strftime(buf, sizeof(buf)-1, fmt, timtm)) {
        fprintf(stderr, "strftime == 0\n");
        exit(1);
    }

    return smprintf("%s", buf);
}

void
setstatus(char *str)
{
    XStoreName(dpy, DefaultRootWindow(dpy), str);
    XSync(dpy, False);
}

char *
loadavg(void)
{
    double avgs[3];

    if (getloadavg(avgs, 3) < 0) {
        perror("getloadavg");
        exit(1);
    }

    return smprintf("%.2f %.2f %.2f", avgs[0], avgs[1], avgs[2]);
}

char*
runcmd(char* cmd) {
    FILE* fp = popen(cmd, "r");
    if (fp == NULL) return NULL;
    char ln[30];
    fgets(ln, sizeof(ln)-1, fp);
    pclose(fp);
    ln[strlen(ln)-1]='\0';
    return smprintf("%s", ln);
}

static unsigned long long lastTotalUser[4], lastTotalUserLow[4], lastTotalSys[4], lastTotalIdle[4];
    
char trash[5];

void initcore(){
        FILE* file = fopen("/proc/stat", "r");
                char ln[100];

        for (int i = 0; i < 5; i++) {
            fgets(ln, 99, file);
            if (i < 1) continue;
            sscanf(ln, "%s %Ld %Ld %Ld %Ld", trash, &lastTotalUser[i-1], &lastTotalUserLow[i-1],
                &lastTotalSys[i-1], &lastTotalIdle[i-1]);
        }
     fclose(file);

}
    
void getcore(char cores[4][5]){
        double percent;
        FILE* file;
        unsigned long long totalUser[4], totalUserLow[4], totalSys[4], totalIdle[4], total[4];
    
                char ln[100];

        file = fopen("/proc/stat", "r");
        for (int i = 0; i < 5; i++) {
            fgets(ln, 99, file);
            if (i < 1) continue;
            sscanf(ln, "%s %Ld %Ld %Ld %Ld", trash, &totalUser[i-1], &totalUserLow[i-1],
                    &totalSys[i-1], &totalIdle[i-1]);
        }
        fclose(file);
    
        for (int i = 0; i < 4; i++) {
            if (totalUser[i] < lastTotalUser[i] || totalUserLow[i]< lastTotalUserLow[i] ||
                    totalSys[i] < lastTotalSys[i] || totalIdle[i] < lastTotalIdle[i]){
                    //Overflow detection. Just skip this value.
                    percent = -1.0;
            }
            else{
                    total[i] = (totalUser[i] - lastTotalUser[i]) + (totalUserLow[i] - lastTotalUserLow[i]) +
                            (totalSys[i] - lastTotalSys[i]);
                    percent = total[i];
                    total[i] += (totalIdle[i] - lastTotalIdle[i]);
                    percent /= total[i];
                    percent *= 100;
            }
            strcpy(cores[i], smprintf("%d%%", (int)percent));
        }
    
        for (int i = 0; i < 4; i++) {
            lastTotalUser[i] = totalUser[i];
            lastTotalUserLow[i] = totalUserLow[i];
            lastTotalSys[i] = totalSys[i];
            lastTotalIdle[i] = totalIdle[i];
        }
   
}

#define MEMCMD "echo $(free -m | awk '/buffers\\/cache/ {print $3}')M"

int
main(void)
{
    char *status;
    char *date;
    char *tme;
    char cores[4][5];
    char *mem;
    char *mpd;
    initcore();
    if (!(dpy = XOpenDisplay(NULL))) {
        fprintf(stderr, "dwmstatus: cannot open display.\n");
        return 1;
    }
    for (;;sleep(1)) {
        date = mktimes("%a, %d %b", tzpst);
        tme = mktimes("%H:%M", tzpst);
        mpd = get_mpd_status();
        mem = runcmd(MEMCMD);
        getcore(cores);
        status = smprintf("\x02ê \x01%s \x02Ñ\x01 %3s %3s \x02Î\x01 %4s \x02·\x10 %s %s\x01 ", mpd, cores[0], cores[1], mem, date, tme);
        setstatus(status);
        free(date);
        free(mpd);
    }

    XCloseDisplay(dpy);

    return 0;
}
