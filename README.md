DWMSTATUS
=========

This program updates the statusbar in DWM (dynamic window manager).

Instead of using BASH, it's programmed in C to lessen the CPU usage (efficiency is everything).

This project is a fork of the suckless project by the same name.

> <img src="http://i.eho.st/ppb4uvzr.png" />
